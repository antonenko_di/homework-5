const colorButton = document.getElementById("colorButton");
const savedTheme = localStorage.getItem("theme");
if (savedTheme) {
    document.body.className = savedTheme;
}

colorButton.addEventListener("click", () => {
    if (document.body.classList.contains("new-theme")) {
        document.body.className = "blue-theme";
    } else {
        document.body.className = "new-theme";
    }

    localStorage.setItem("theme", document.body.className);
});
